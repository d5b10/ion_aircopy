import java.net._;

object Main {
  
  def main( args: Array[String] ) : Unit = {
    var client: AirCopyClient = new AirCopyClient( InetAddress.getByName("192.168.18.33"))
    
    println( new String(client.getStatus) )
    println( client.setResStd() )
    println( new String(client.startScan) )
    Thread.sleep(1000) // apparently this pause is required, otherwise the getImageData call will hang
    println("I'm back again")
    client.getJPEGData
  }
}