import java.net._
import java.nio.ByteBuffer;
import java.io._

class AirCopyClient( host: InetAddress ) {
  
  var socket: Socket = new Socket( host, 23 )
  
  private val GETDEVVER = 0x30302020
  private val GETSTATUS = 0x00600050
  private val SETRESSTD = 0x40302010
  private val SETRESHIG = 0x80706050
  private val STARTSCAN = 0x00200010
  private val GETIMDATA = 0x40403030
  private val GETJPEGSZ = 0x00D000C0
  private val GETJPEGDT = 0x00F000E0
  
  def sendMsg( msg: Int ) : Unit = {
    var s = socket.getOutputStream
    var bb = ByteBuffer.allocate(4)
    bb.putInt(msg)
    s.write( bb.array )
    s.flush
  }
  
  def rcvStdAnswer(len: Byte) : Array[Byte] = {
    var result = new Array[Byte](len)
    socket.getInputStream.read(result)
    return result
  }
  
  private def rcvBlobAnswer(l : BigInt) : File = {
      var fn = File.createTempFile("IonAirCopyClient", ".rgb")
      println("Output file "+fn)
      var wrt = new FileOutputStream( fn )
      var result = new Array[Byte](1024)
      var read : BigInt = 0
      while ( read < l ) {
        var r = socket.getInputStream.read(result)
        wrt.write(result, 0, r)
        read += r
      }
      wrt.flush()
      return fn
  }
  
  def setResHigh() : Boolean = {
    sendMsg( this.SETRESHIG )
    var result: Array[Byte] = rcvStdAnswer(11) 
    if ( new String(result).startsWith("dpifine") )
      return true
    else
      return false
  }
  
  def setResStd() : Boolean = {
    sendMsg( this.SETRESSTD )
    var result: Array[Byte] = rcvStdAnswer(11) 
    if ( new String(result).startsWith("dpistd") )
      return true
    else
      return false
  }
  
  def getStatus : Array[Byte] = {
    sendMsg( GETSTATUS )
    return rcvStdAnswer(11)
  }
  
  def startScan : Array[Byte] = {
    sendMsg( STARTSCAN )
    return rcvStdAnswer(11)
  }
  
//  def getImageData : File = {
//    sendMsg( GETIMDATA )
//    return rcvBlobAnswer()
//  }
//  
  def getJPEGSize : Int = {
    sendMsg( GETJPEGSZ ) 
    var x = rcvStdAnswer(12)
    var bb = ByteBuffer.allocate(4)
    bb.put(x(11))
    bb.put(x(10))
    bb.put(x(9))
    bb.put(x(8))
    return bb.getInt(0)
  }
  
  def getJPEGData : File = {
    var l = getJPEGSize
    sendMsg( GETJPEGDT )
    return rcvBlobAnswer(l)
  }
  
}
