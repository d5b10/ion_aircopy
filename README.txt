ION_AicCopy - a Scala client for the ION Aircopy scanner

This is my attempt to write a JVM-based client for the ION AirCopy scanner. Scanning documents works, but I stopped working on it when I realized, someone else had already done it (http://bastel.dyndns.info/~public/s400w/), but it was still fun and I learned some Scala!

The Protocol

The scanner provides a WiFi access point (named DIRECT-xxxxxx_AirCopy - the naming scheme is obviously
checked by the app), that clients need to connect to. They will be given an IP address in the private
network 192.168.18.x with .33 being the scanner). The client initiates the communication through a 
TCP connection to port 23 (telnet) on the scanner's IP. Requests from the client are always 4 bytes long
(non readable) and most responses are 11 bytes long (mostly human readable and the last byte is always
0x48 - except the "jpegsize" answer, see below).

Here are the client requests that I encountered while sniffing some scans:

0x30 0x30 0x20 0x20		Get the firmware version of the scanner (sth like IO0a.032).

0x00 0x60 0x00 0x50		This is a request for the scanner's status and is always issued in a seperate
						connection by the app (not sure if this is important). Possible answers are:
						
							"scanready" 0x00 0x48 - this obviously means that the scanner is ready
							"nopaper" 0x00 0x00 0x00 0x48 - no paper to scan was inserted into the scanner

0x40 0x30 0x30 0x10		This sets the scanner to standard resolution, the answer should be:

							"dpistd" 0x00 0x00 0x00 0x00 0x48
														
0x80 0x70 0x60 0x50		This sets the scanner to fine resolution, the answer should be:

							"dpifine" 0x00 0x00 0x00 0x48
							
0x00 0x20 0x00 0x10		This starts the scan and returns one of these values:

							"scango" 0x00 0x00 0x00 0x00 0x48 - scan was started
							"nopaper" 0x00 0x00 0x00 0x48 - no paper to scan was inserted into the scanner
							
0x40 0x40 0x30 0x30		This retrieves a preview of the scanned image. I think it is fixed in size.

0x00 0xD0 0x00 0xC0		get the size of the full-resolution JPEG

							"jpegsize" and 4 more bytes (a little-endian int)

0x00 0xF0 0x00 0xE0		get the JPEG image. For some reason there is a leading 0 byte which
						needs to be removed to make it a valid JPEG.

Author

Daniel Boesswetter <daniel at daniel-boesswetter.de>